#import <Flutter/Flutter.h>

@interface PendoFlutterPlugin : NSObject<FlutterPlugin>
+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry NS_SWIFT_NAME(registerWithRegistry(_:));
@end
