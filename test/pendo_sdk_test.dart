import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pendo_sdk/pendo_sdk.dart';

void main() {
  const MethodChannel channel = MethodChannel('pendo_sdk');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

//  test('getPlatformVersion', () async {
//    //expect(await PendoFlutterPlugin.platformVersion, '42');
//  });
}
